package com.gmarvin.demo_app_kotlin.view.product

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gmarvin.demo_app_kotlin.R
import com.gmarvin.demo_app_kotlin.provider.MyApiAdapterProvider
import com.gmarvin.demo_app_kotlin.response.DataProductResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ProductActivity : AppCompatActivity() {

    var productAdapter: ProductAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        productAdapter = ProductAdapter(this, ArrayList())

        getData()

        val rvProducts = findViewById<RecyclerView>(R.id.rvProducts)

        rvProducts.layoutManager = GridLayoutManager(this, 2)
        rvProducts.adapter = productAdapter

    }

    private fun getData() {
        MyApiAdapterProvider().getApiService()?.getProducts()
            ?.enqueue(object : Callback<DataProductResponse> {
                override fun onResponse(call: Call<DataProductResponse>, response: Response<DataProductResponse>) {
                    if (response.isSuccessful && response.body() != null) {
                        productAdapter?.setProductList(response.body()!!.products)
                    } else {
                        Toast.makeText(applicationContext, "Ah ocurrido un error", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<DataProductResponse>, t: Throwable) {
                    Toast.makeText(applicationContext, "Ah ocurrido un error", Toast.LENGTH_SHORT).show()
                }
            })
    }

}