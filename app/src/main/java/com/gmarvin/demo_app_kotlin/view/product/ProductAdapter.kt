package com.gmarvin.demo_app_kotlin.view.product

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gmarvin.demo_app_kotlin.R
import com.gmarvin.demo_app_kotlin.response.ProductResponse

class ProductAdapter(
    private val mContext: Context,
    productList: List<ProductResponse>
) :
    RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    private var productList: List<ProductResponse>

    fun setProductList(productList: List<ProductResponse>?) {
        if (productList != null) {
            this.productList = productList
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pr: ProductResponse = productList[position]
        holder.tvName.text = pr.name
        holder.tvDescription.text = pr.description
        holder.tvPrice.text = "NIO " + pr.price
        if (pr.image != null) {
            Glide.with(mContext).load(pr.image).centerCrop().into(holder.ivProduct)
        }
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cvItem: CardView
        var ivProduct: ImageView
        var tvName: TextView
        var tvDescription: TextView
        var tvPrice: TextView
        var ibShopping: ImageButton
        var ibShopping2: ImageButton

        init {
            cvItem = itemView.findViewById(R.id.cvItem)
            ivProduct = itemView.findViewById(R.id.ivProduct)
            tvName = itemView.findViewById(R.id.tvName)
            tvDescription = itemView.findViewById(R.id.tvDescription)
            tvPrice = itemView.findViewById(R.id.tvPrice)
            ibShopping = itemView.findViewById(R.id.ibShopping)
            ibShopping2 = itemView.findViewById(R.id.ibShopping2)
        }
    }

    init {
        this.productList = productList
    }
}