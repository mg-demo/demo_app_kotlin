package com.gmarvin.demo_app_kotlin

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gmarvin.demo_app_kotlin.provider.MyApiAdapterProvider
import com.gmarvin.demo_app_kotlin.view.product.ProductActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            startActivity(Intent(applicationContext, ProductActivity::class.java))
        }

        MyApiAdapterProvider().getApiServiceProvider()

    }

}