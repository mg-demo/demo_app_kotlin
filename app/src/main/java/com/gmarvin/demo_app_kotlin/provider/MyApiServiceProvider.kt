package com.gmarvin.demo_app_kotlin.provider

import com.gmarvin.demo_app_kotlin.response.DataProductResponse
import retrofit2.Call
import retrofit2.http.GET

interface MyApiServiceProvider {

    @GET("products")
    fun getProducts(): Call<DataProductResponse>

}