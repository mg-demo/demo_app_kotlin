package com.gmarvin.demo_app_kotlin.provider

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MyApiAdapterProvider {

    var API_SERVICE: MyApiServiceProvider? = null

    fun getApiServiceProvider(): MyApiServiceProvider? {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)

        val baseUrl = "http://192.168.1.2:8000/api/"
        if (API_SERVICE == null) {
            val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build()
            API_SERVICE = retrofit.create(MyApiServiceProvider::class.java)
        }

        return API_SERVICE
    }

    fun getApiService(): MyApiServiceProvider? {
        return API_SERVICE
    }

}