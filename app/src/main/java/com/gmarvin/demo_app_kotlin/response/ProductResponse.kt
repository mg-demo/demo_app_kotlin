package com.gmarvin.demo_app_kotlin.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProductResponse {

    @SerializedName("id")
    @Expose val id = 0
    @SerializedName("code")
    @Expose val code: String? = null
    @SerializedName("name")
    @Expose val name: String? = null
    @SerializedName("description")
    @Expose val description: String? = null
    @SerializedName("price")
    @Expose val price = 0.0
    @SerializedName("image")
    @Expose val image: String? = null
    @SerializedName("created_at")
    @Expose val createdAt: String? = null
    @SerializedName("updated_at")
    @Expose val updatedAt: String? = null

    override fun toString(): String {
        return "ProductResponse(id=$id, code=$code, name=$name, description=$description, price=$price, image=$image, createdAt=$createdAt, updatedAt=$updatedAt)"
    }

}