package com.gmarvin.demo_app_kotlin.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DataProductResponse {

    @SerializedName("result")
    @Expose val result: String? = null

    @SerializedName("message")
    @Expose val message: String? = null

    @SerializedName("products")
    @Expose var products: List<ProductResponse>? = null

    override fun toString(): String {
        return "DataProductResponse(result=$result, message=$message, products=$products)"
    }

}